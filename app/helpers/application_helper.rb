module ApplicationHelper
  def current_controller_title
      current_controller = params[:controller]
      current_controller.gsub!('_',' ')
      current_controller.gsub!(/\b\w/){$&.upcase}
  end
end
