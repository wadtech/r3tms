jQuery ->
  consumables = $('#order_consumable_id').html()
  consumable_default = $('#order_consumable_id :selected').text()
  $('#order_consumable_id').empty().append("<option>#{consumable_default}</option>")
  console.log(consumables)
  $('#order_printer_models').change ->
    printer = $('#order_printer_models :selected').text()
    escaped_printer = printer.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    console.log(printer)
    options = $(consumables).filter("optgroup[label=#{escaped_printer}]").html()
    console.log(options)
    if options
      $('#order_consumable_id').html(options).prepend("<option>Please select your consumable...</option>")
    else
      $('#order_consumable_id').empty().append("<option>#{consumable_default}</option>")

  $("td.pending-time:contains('hour')").each ->
    $(this).parent().addClass('warn')
  $("td.pending-time:contains('day')").each ->
    $(this).parent().addClass('warn')
  $("td").each -> 
    if $(this).hasClass("DEL")
      $(this).parent().addClass('grey-out')
    else if $(this).hasClass("DIS")
      $(this).parent().addClass('grey-out green-out')
    else if $(this).hasClass("PEN")
      $(this).parent().addClass('red-out')