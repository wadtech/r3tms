# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  $('.selectable').click ->
    console.log("selectable clicked!")
    $('.selectable').each ->
      console.log("removing selected class")
      $(this).children('.printer-thumb').removeClass("selected")
    $(':checked').each ->
      console.log("selected added!")
      $(this).parent('.printer-thumb').addClass("selected")