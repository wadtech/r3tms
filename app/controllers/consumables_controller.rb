class ConsumablesController < ApplicationController
  before_filter :authenticate_admin!, :get_all_batches

  # GET /consumables
  # GET /consumables.json
  def index
    @consumables = Consumable.paginate(:page => params[:page] || 1, :per_page => 10).order('description DESC')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @consumables }
    end
  end

  # GET /consumables/1
  # GET /consumables/1.json
  def show
    @consumable = Consumable.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @consumable }
    end
  end

  # GET /consumables/new
  # GET /consumables/new.json
  def new
    get_all_batches
    @consumable = Consumable.new
    @price = BatchPrice.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @consumable }
    end
  end

  # GET /consumables/1/edit
  def edit
    @consumable = Consumable.find(params[:id])
    @price = @consumable.batch_price
  end

  # POST /consumables
  # POST /consumables.json
  def create
    @consumable = Consumable.new(params[:consumable].except(params[:consumable][:batch]))
    @price = @consumable.build_batch_price(
        :batch_id => Batch.find(params[:batch_price][:batch]),
        :consumable => @consumable,
        :price => params[:batch_price][:price],
        :stock => params[:batch_price][:stock]
      )

    respond_to do |format|
      if @consumable.save
        format.html { redirect_to consumables_path,
                      notice: 'Consumable was successfully created.'
                    }
        format.json { render json: @consumable, status: :created, location: @consumable }
      else
        format.html { render action: "new" }
        format.json { render json: @consumable.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /consumables/1
  # PUT /consumables/1.json
  def update
    @consumable = Consumable.find(params[:id])
    # @consumable.batch_price.batch = Batch.find(params[:batch_price][:batch])
    @consumable.update_attributes(params[:consumable])

    respond_to do |format|
      if @consumable.save
        format.html { redirect_to consumables_path,
                      notice: "Consumable was successfully updated."
                    }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @consumable.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /consumables/1
  # DELETE /consumables/1.json
  def destroy
    @consumable = Consumable.find(params[:id])
    @consumable.destroy

    respond_to do |format|
      format.html { redirect_to consumables_url }
      format.json { head :ok }
    end
  end

  protected
  def get_all_batches
    @batches = Batch.all
  end
end
