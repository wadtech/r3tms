class PrinterModelsController < ApplicationController
  before_filter :authenticate_admin!
  
  # GET /printer_models
  # GET /printer_models.json
  def index
    get_all_manufacturers
    @printer_models = PrinterModel.paginate page: params[:page], order: 'name desc', per_page: 10

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @printer_models }
    end
  end

  # GET /printer_models/1
  # GET /printer_models/1.json
  def show
    get_all_manufacturers
    @printer_model = PrinterModel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @printer_model }
    end
  end

  # GET /printer_models/new
  # GET /printer_models/new.json
  def new
    get_all_manufacturers
    @printer_model = PrinterModel.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @printer_model }
    end
  end

  # GET /printer_models/1/edit
  def edit
    get_all_manufacturers
    @printer_model = PrinterModel.find(params[:id])
  end

  # POST /printer_models
  # POST /printer_models.json
  def create
    get_all_manufacturers
    @printer_model = PrinterModel.new(params[:printer_model])

    respond_to do |format|
      if @printer_model.save
        format.html { redirect_to printer_models_path, notice: "#{Manufacturer.find_by_id(@printer_model.manufacturer_id).name} #{@printer_model.name} was successfully created." }
        format.json { render json: @printer_model, status: :created, location: @printer_model }
      else
        format.html { render action: "new" }
        format.json { render json: @printer_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /printer_models/1
  # PUT /printer_models/1.json
  def update
    get_all_manufacturers
    @printer_model = PrinterModel.find(params[:id])

    respond_to do |format|
      if @printer_model.update_attributes(params[:printer_model])
        format.html { redirect_to printer_models_path, notice: "#{Manufacturer.find_by_id(@printer_model.manufacturer_id).name} #{@printer_model.name} was successfully updated." }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @printer_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /printer_models/1
  # DELETE /printer_models/1.json
  def destroy
    @printer_model = PrinterModel.find(params[:id])
    @printer_model.destroy

    respond_to do |format|
      format.html { redirect_to printer_models_url }
      format.json { head :ok }
    end
  end
  
  protected
  def get_all_manufacturers
    @manufacturers = Manufacturer.all
  end
end
