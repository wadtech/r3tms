class OrdersController < ApplicationController
  before_filter :authenticate_admin!, :except => [:new, :create]

  layout 'application'
  layout 'front', :only => :new

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.paginate page: params[:page], order: 'updated_at desc', per_page: 50

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @order }
    end
  end
  
  # GET /orders/track
  # GET /orders/track.json
  def track 
    @pending = Order.pending
    @awaiting = Order.awaiting
    @dispatched = Order.dispatched
    @deleted = Order.deleted

    respond_to do |format|
      format.html
      format.json { render json: @order }
    end
  end

  # PUT /orders/dispatch
  # PUT /orders/dispatch.json
  def complete
    @order = Order.find(params[:id])
    @order.order_status = "DIS"
    @order.adjust_stock!(-1)
    
    respond_to do |format|
      if @order.save
        format.html { 
          redirect_to tracking_path, 
          :notice => "#{@order.consumable.full_name} for #{@order.printer_names} Dispatched to #{@order.location}"
        }
        format.json { render json: @order }
      else
        @order.adjust_stock!(+1)
        format.html {
          redirect_to tracking_path,
          flash.now[:error] = "Error dispatching consumable, are there any in stock?"
        }
      end
    end
  end

  # GET /orders/new
  # GET /orders/new.json
  def new
    @order = Order.new
    @printers = PrinterModel.all.sort
    @consumables = Consumable.all
    @departments = Department.order(:name)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/1/edit
  def edit
    @order = Order.find(params[:id])
    @printers = PrinterModel.all.sort
    @consumables = Consumable.all
    @departments = Department.order(:name)
  end

  # POST /orders
  # POST /orders.json
  def create
    params[:order] = params[:order].except(:printer_models)
    @order = Order.new(params[:order])
    @order.department = Department.find(params[:order][:department_id])
    @order.consumable = Consumable.find(params[:order][:consumable_id])
    @order.calculate_order_status
    @order.price = @order.consumable.batch_price.price
    
    respond_to do |format|
      if @order.save
        format.html { redirect_to root_url, :flash => { :result => 'sent' }, notice: 'Order was successfully created.' }
        format.json { render json: @order, status: :created, location: root_url }
      else
        format.html {
          flash.now[:error] = "There was an error"
          render :new 
        }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /orders/1
  # PUT /orders/1.json
  def update
    @order = Order.find(params[:id])

    respond_to do |format|
      if @order.update_attributes(params[:order])
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    #Orders are never destroyed, this action instead sets it to a DELETED state.

    @order = Order.find(params[:id])
    @order.order_status = "DEL"

    respond_to do |format|
      if @order.save
        format.html { redirect_to orders_url }
        format.json { head :ok }
        format.js
      else
        raise
      end
    end
  end
end
