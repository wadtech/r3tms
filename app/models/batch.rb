# == Schema Information
#
# Table name: batches
#
#  id           :integer          not null, primary key
#  batch_date   :date
#  created_at   :datetime
#  updated_at   :datetime
#  batch_id     :string(255)
#  supplier_ref :string(255)
#

class Batch < ActiveRecord::Base
  has_many :batch_prices
  has_many :consumables, :through => :batch_prices
  validates :batch_date, :batch_id, :presence => true
  validates :batch_id, :uniqueness => true, :format => { :with => /\bae(\w*)|AE(\w*)\b/ }

  attr_accessible :batch_id, :supplier_ref, :batch_date
end
