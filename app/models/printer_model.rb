# == Schema Information
#
# Table name: printer_models
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  manufacturer_id    :integer
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

class PrinterModel < ActiveRecord::Base
  has_many :orders
  has_and_belongs_to_many :consumables
  belongs_to :manufacturer, :dependent => :destroy

  validates :name, :manufacturer, :presence => true
  validates :name, :uniqueness => true

  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }

  attr_accessor :full_name
  attr_accessible :name, :image, :manufacturer_id

  def full_name
    "#{self.manufacturer.name} #{name}"
  end

  def consumable_count
    "#{self.consumables.count}"
  end

  #Allow sort by full_name
  def <=> (obj2)
    self.full_name <=> obj2.full_name
  end
  def < (obj2)
    self.full_name < obj2.full_name
  end
  def > (obj2)
    self.full_name > obj2.full_name
  end
  def == (obj2)
    self.full_name = obj2.full_name
  end
end
