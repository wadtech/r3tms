# == Schema Information
#
# Table name: consumables
#
#  id          :integer          not null, primary key
#  description :string(255)
#  reorder_no  :string(255)
#  kind        :string(255)
#  supplier_id :integer
#  order_id    :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Consumable < ActiveRecord::Base
  has_and_belongs_to_many :printer_models
  belongs_to :supplier
  has_many :manufacturers, :through => :printer_models
  has_many :orders
  has_one :batch_price
  has_one :batch, :through => :batch_price

  attr_accessible :description, :reorder_no, :kind, :supplier_id, :order_ids, :batch, :printer_model_ids

  accepts_nested_attributes_for :batch_price

  KINDS = [ "REM", "REM-HICAP", "OEM", "OEM-HICAP" ]
  validates :description, :reorder_no, :batch_price, :presence => true
  validates :kind, inclusion: KINDS

  def full_name
    "#{self.supplier.name}/#{reorder_no}"
  end

  def effective_stock
    self.batch_price.stock - Order.where(:consumable_id => self.id, :order_status => "PEN").count
  end

  def description_with_stock
    if self.batch_price.stock >= 1 
      "#{self.description} (#{self.effective_stock} available)" 
    else
      "#{self.description} (out of stock)" 
    end
  end

  def grouped_description_with_stock
    effective_stock_total = 0
    Consumable.all.each do |consumable|
      begin
        effective_stock_total += consumable.effective_stock
      end unless ((consumable.printer_model_ids & self.printer_model_ids).empty?) or (consumable.description != self.description)
    end

    if effective_stock_total >= 1 
      "#{self.description} (#{effective_stock_total} available)"
    else
      "#{self.description} (out of stock)" 
    end
  end
end
