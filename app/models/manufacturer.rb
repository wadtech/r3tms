# == Schema Information
#
# Table name: manufacturers
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Manufacturer < ActiveRecord::Base
  has_many :printer_models

  validates :name, :presence => true, :uniqueness => true

  attr_accessible :name

  def count_printer_models
    printer_models.size
  end
end
