# == Schema Information
#
# Table name: batch_prices
#
#  id            :integer          not null, primary key
#  batch_id      :integer
#  consumable_id :integer
#  price         :decimal(3, 2)
#  stock         :integer
#

class BatchPrice < ActiveRecord::Base
  belongs_to :batch, :foreign_key => "batch_id"
  belongs_to :consumable, :foreign_key => "consumable_id"
  validates :batch_id, :consumable, :price, :stock, :presence => true

  attr_accessible :price, :consumable, :stock, :batch_id, :consumable_id
end
