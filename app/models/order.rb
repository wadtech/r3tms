# == Schema Information
#
# Table name: orders
#
#  id            :integer          not null, primary key
#  location      :string(255)
#  price         :decimal(3, 2)
#  order_status  :string(255)
#  department_id :integer
#  consumable_id :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class Order < ActiveRecord::Base
  attr_accessible :location, :department_id, :consumable_id, :printer_models

  belongs_to :department
  belongs_to :consumable
  has_many :printer_models, :through => :consumable

  # Possible order statuses, don't forget to add relevent scope!

  STATUS = {
              "PEN" => "Pending Dispatch",
              "AWS" => "Awaiting Stock",
              "DIS" => "Dispatched",
              "DEL" => "Deleted",
              "FOC" => "Free of Charge",
              "ADD" => "Added to Stock"
            }

  validates :location, :price, :order_status, :department_id, :consumable_id, :presence => true
  validates :order_status, :inclusion => { :in => STATUS }

  #scopes
  scope :status, ->(stat) { where("orders.order_status LIKE ?", stat) }

  scope :pending, order.status("PEN")
  scope :awaiting, order.status("AWS")
  scope :deleted, order.status("DEL")
  scope :dispatched, order.status("DIS")
  scope :free, order.status("FOC")
  scope :added, order.status("ADD")

  def calculate_order_status
    stock = self.consumable.batch_price.stock
    if stock >= 1
      self.order_status = "PEN"
    elsif stock == 0
      self.order_status = "AWS"
    end
  end

  def order_status_text
    STATUS[order_status]
  end

  def consumable_name(type=false)
    consumable = Consumable.find(self.consumable_id)
    type ? "#{consumable.full_name}, #{consumable.description} (#{consumable.kind})" : "#{consumable.full_name}, #{consumable.description}"
  end

  def printer_names
    result = []
    self.consumable.printer_models.each do |printer|
      result << printer.full_name
    end
    result.to_sentence
  end

  def adjust_stock!(amount)
    @batch_price = BatchPrice.where(:consumable_id => self.consumable.id).first
    @batch_price.stock += amount
    @batch_price.save!
  end
end
