# == Schema Information
#
# Table name: suppliers
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Supplier < ActiveRecord::Base
  has_many :consumables

  validates :name, :presence => true, :uniqueness => true

  attr_accessible :name
end
