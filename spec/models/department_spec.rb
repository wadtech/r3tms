# == Schema Information
#
# Table name: departments
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Department do

  it "should have a valid factory" do
    FactoryGirl.build(:department).should be_valid
  end

  it "should have a unique name" do
    FactoryGirl.create(:department, :name => 'duplicated name')
    FactoryGirl.build(:department, :name => 'duplicated name').should_not be_valid
  end

  it "cannot have a blank name" do
    FactoryGirl.build(:department, :name => "").should_not be_valid
  end
end
