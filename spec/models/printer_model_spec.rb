# == Schema Information
#
# Table name: printer_models
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  manufacturer_id    :integer
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

require 'spec_helper'

describe PrinterModel do
  it "should have a valid factory" do
    FactoryGirl.build(:printer_model).should be_valid
  end

  it "should have a unique name" do
    FactoryGirl.create(:printer_model, :name => "Printer")
    FactoryGirl.build(:printer_model, :name => "Printer").should_not be_valid
  end

  it "must belong to a manufacturer" do
    FactoryGirl.build(:printer_model, :manufacturer => nil).should_not be_valid
  end
end
