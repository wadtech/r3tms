# == Schema Information
#
# Table name: admins
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(128)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#

require 'spec_helper'

describe Admin do

  it "should have a valid factory" do
    FactoryGirl.build(:admin).should be_valid
  end

  context "validations" do
    describe "email address" do
      it "should be present and valid" do
        FactoryGirl.build(:admin, :email => "").should_not be_valid

        %w[admin@foo.com THE_USER@foo.bar.org first.last@foo.jp].each do |address|
          FactoryGirl.build(:admin, :email => address).should be_valid
        end

        %w[foo.com s!!!omerubbish h12362tgjdkgav3n@1234].each do |address|
          FactoryGirl.build(:admin, :email => address).should_not be_valid
        end
      end

      it "should reject duplicates ignoring case" do
        FactoryGirl.create(:admin, :email => "hello@test.com")
        FactoryGirl.build(:admin, :email => "hello@test.com").should_not be_valid
        FactoryGirl.build(:admin, :email => "HELLO@TEST.COM").should_not be_valid
      end
    end

    describe "passwords" do
      it "should have a password attribute and it cannot be nil" do
        FactoryGirl.build(:admin).should respond_to(:password)
        FactoryGirl.build(:admin).should respond_to(:password_confirmation)
        FactoryGirl.build(:admin, :password => nil, :password_confirmation => nil).should_not be_valid
      end

      it "should require a password with a matching password confirmation" do
        FactoryGirl.build(:admin, :password => nil, :password_confirmation => nil).should_not be_valid
        FactoryGirl.build(:admin, :password_confirmation => "invalid").should_not be_valid
      end

      it "should reject passwords shorter than 6 characters" do
        short = "a" * 5
        FactoryGirl.build(:admin, :password => short, :password_confirmation => short).should_not be_valid
      end

      it "should have a non-nil encrypted password attribute" do
        FactoryGirl.build(:admin).should respond_to(:encrypted_password)
        FactoryGirl.build(:admin).encrypted_password.should_not be_blank
        FactoryGirl.build(:admin, :encrypted_password => nil).should_not be_blank
      end
    end
  end
end
