# == Schema Information
#
# Table name: manufacturers
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Manufacturer do
  it "should have a valid factory" do
    FactoryGirl.build(:manufacturer).should be_valid
  end

  it "must have a unique, non-nil name" do
    FactoryGirl.create(:manufacturer, :name => "duplicate name")
    FactoryGirl.build(:manufacturer, :name => "duplicate name").should_not be_valid
    FactoryGirl.build(:manufacturer, :name => '').should_not be_valid
  end
end
