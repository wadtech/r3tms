# == Schema Information
#
# Table name: batch_prices
#
#  id            :integer          not null, primary key
#  batch_id      :integer
#  consumable_id :integer
#  price         :decimal(3, 2)
#  stock         :integer
#

require 'spec_helper'

describe BatchPrice do
  describe "factory" do
    before :each do
      @batchprice = FactoryGirl.build(:batch_price)
    end

    it "should have a valid factory" do
      @batchprice.should be_valid
    end

    it "should create a valid consumable" do
      @batchprice.consumable.should be_valid
    end

    it "should create a valid batch" do
      @batchprice.batch.should be_valid
    end
  end

  # Associations
  it { should belong_to :consumable }
  it { should belong_to :batch }

end
