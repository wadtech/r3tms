# == Schema Information
#
# Table name: suppliers
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Supplier do

  it "should have a valid factory" do
    FactoryGirl.build(:supplier).should be_valid
  end

  it "should have a unique name" do
    FactoryGirl.create(:supplier, :name => "Supplier")
    FactoryGirl.build(:supplier, :name => "Supplier").should_not be_valid
  end

  it "cannot have a blank name" do
    FactoryGirl.build(:supplier, :name => "").should_not be_valid
  end
end
