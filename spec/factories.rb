require 'factory_girl'

FactoryGirl.define do
  factory :admin do
    sequence(:email) { |n| "test#{n}@test.com" }
    password 'longpassword'
    password_confirmation { |u| u.password }
  end

  factory :manufacturer do
    sequence(:name) { |n| "Manufacturer#{n}" }
  end

  factory :supplier do
    sequence(:name) { |n| "Supplier#{n}" }
  end

  factory :printer_model do |p|
    p.manufacturer
    p.sequence(:name) { |n| "Printer#{n}" }

    factory :printer_model_with_consumables do |pc|
      pc.association(:consumable)
    end
  end

  factory :department do
    sequence(:name) { |n| "Department#{n}" }
  end

  factory :batch do
    batch_date Date.today
    sequence(:batch_id) { |n| "AE#{n}#{n+1}#{n}#{n+3}" }
  end

  factory :batch_price do |bp|
    bp.batch
    bp.consumable
    bp.stock Random.rand(0...10)
    bp.price [Random.rand(0...150), Random.rand(0...99)].join('.')
  end

  factory :consumable do |c|
    c.description 'Sample Toner Description'
    c.sequence(:reorder_no) { |n| "#{n}" }
    c.kind 'OEM'
    c.supplier
  end
end