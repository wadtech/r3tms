require 'spec_helper'
# This spec is for admin functions such as adding resources to database and deleting them

describe "adding a manufacturer" do
  self.use_transactional_fixtures = true
  @user = FactoryGirl.create :admin

  it "allows a logged in user to create a new record" do
    visit root_url
    login_admin(@user, manufacturers_url)
    click_link "New Manufacturer"
    page.should have_button "Create"

    fill_in "Name", :with => "Test Manufacturer"
    click_button "Create"

    page.should have_content "successfully"
    page.should have_content "Test Manufacturer"
  end
end
