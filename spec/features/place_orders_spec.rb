require 'spec_helper'

# This request spec tests whether a user who is not logged on can successfully place an order.
# It also tests for an incomplete form displaying the correct text.

describe "Placing an order" do
  self.use_transactional_fixtures = false

  it "works for any user" do
    FactoryGirl.create(:printer_model_with_consumable)
    FactoryGirl.create(:department)
    FactoryGirl.create(:batch_price)

    visit root_url
    fill_in "Location", :with => "E8"
    select_second_option "order_department_id"
    select_second_option "order_printer_models"
    select 'Sample Toner Description', from: 'order_consumable_id'
    save_and_open_page
    click_button "Place Order"

    page.should have_content "Your consumable request has been received"
  end

  it "fails when fields are incomplete" do
    visit root_url
    fill_in "Location", :with => "E8"
    click_button "Place Order"
    save_and_open_page
    page.should have_content "Sorry"
    page.should have_content "Please select a printer first."
  end

end
