require 'spec_helper'

describe "creating a batch" do
  self.use_transactional_fixtures = true

  before :all do
    @user = FactoryGirl.create(:admin)
  end

  context "logged in" do
    it "is allowed for an admin" do
      visit root_url
      login_admin(@user, batches_url)
      click_link "New Batch"
      page.should have_button "Create"

      fill_in "batch_batch_id", :with => "AE040252"
      fill_in "batch_supplier_ref", :with => "234del"

      click_button "Create Batch"
      page.should have_content "created"

      click_link "consumables"
      click_link "New Consumable"
      page.should have_content "Create"
    end
  end
end

