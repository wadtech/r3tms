source 'http://rubygems.org'

gem 'rails', '3.2.9'
gem 'jquery-rails'
gem 'will_paginate', '~> 3.0'
gem 'execjs'

# User authentication
gem 'devise', '>= 2.0.0'
gem 'bcrypt-ruby', '~> 3.0.0'

# Deployment
gem 'capistrano'
gem 'rvm-capistrano', '>= 1.1.0'

# Settings
gem 'settingslogic'

# Bootstrap
gem "therubyracer" unless RUBY_PLATFORM=~ /mingw32/
gem "less-rails" #Sprockets (what Rails 3.1 uses for its asset pipeline) supports LESS
gem "twitter-bootstrap-rails"

# Printer Images
if RUBY_PLATFORM=~ /mingw32/
  gem 'paperclip', '>= 3.2', :git => "https://github.com/thoughtbot/paperclip.git"
  gem 'cocaine', '>= 0.0.2', :git => "https://github.com/thoughtbot/cocaine.git"
else
  gem 'paperclip', '>= 3.2', :github => "thoughtbot/paperclip"
  gem 'cocaine', '>= 0.0.2', :github => "thoughtbot/cocaine"
end

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.5'
  gem 'coffee-rails', '~> 3.2.2'
  gem 'uglifier', '>= 1.0.3'
end

group :test do
  gem "ZenTest"
  gem "autotest-growl"
  gem "autotest-rails-pure"
  gem 'autotest-notification'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'capybara'
  gem "launchy"
  gem 'shoulda-matchers'
  gem 'simplecov', :require => false
end

group :development, :test do
  gem 'rspec-rails'
  gem 'sqlite3'
  gem "annotate", "~> 2.5.0"
  gem 'thin'
end

group :production do
  gem 'mysql2'
  gem 'unicorn', :platforms => :ruby
end