# Manufacturers

Manufacturer.delete_all

%w{ Brother HP Konica\ Minolta }.inject(1) do | count, name |
  Manufacturer.create! do |m|
    m.name = name 
    m.id = count
  end
  puts "Manufacturer #{name} with ID #{count} created."
  count += 1
end
@@manufacturer_count = Manufacturer.count

# Suppliers
Supplier.delete_all

%w{ IJT BIST }.inject(1) do | count, name |
  Supplier.create! do |s|
  s.name = name
  s.id = count
  end
  puts "Supplier #{name} with ID #{count} created."
  count += 1
end
@@supplier_count = Supplier.count

# Printers

PrinterModel.delete_all

%w{ HL-5350DN HL-5380DN }.inject(4) do | count, name |
  printer = PrinterModel.create! do |m|
    m.name = name
    m.manufacturer = Manufacturer.where(:id => 1).first
    m.id = count
  end
  puts "#{printer.full_name} created."
  count += 1
end

%w{ 1600 2600 2600n }.inject(1) do | count, name |
  printer = PrinterModel.create! do |m|
    m.name = "LaserJet #{name}"
    m.manufacturer = Manufacturer.where(:id => 2).first
    m.id = count
  end
  puts "#{printer.full_name} created."
  count += 1
end
@@printer_count = PrinterModel.count

# Batches
Batch.delete_all

%w{ AE123442 AE123984 AE293482 }.each do | id |
  Batch.create! do |b|
    b.batch_date = Time.now.to_date
    b.batch_id = "#{id}"
  end
  puts "Added batch #{id}."
end
@@batch_count = Batch.count

# Consumables
Consumable.delete_all

def seed_consumable(opts = {})
  Consumable.create! do |c|
    c.batch = Batch.where(:id => Random.rand(1...@@batch_count)).first
    c.description = opts[:desc]
    c.reorder_no = opts[:order]
    c.kind = opts[:kind]
    c.printer_models = PrinterModel.where(:id => opts[:printers])
    c.supplier = Supplier.where(:id => Random.rand(1..@@supplier_count)).first
    c.build_batch_price do | p |
      p.batch = c.batch
      p.consumable = c
      p.price = "#{Random.rand(0..999)}.#{Random.rand(0..99)}"
      p.stock = Random.rand(0..20)
    end
  end
  puts "#{opts[:desc]} (#{opts[:kind]}) added."
end
seed_consumable ({:desc => "Black Toner", :order => Random.rand(1000..1049), :kind => "OEM", :printers => [4,5]})
seed_consumable ({:desc => "Cyan Toner", :order => Random.rand(1050..1099), :kind => "REM", :printers => [1,2,3]})
seed_consumable ({:desc => "Magenta Toner", :order => Random.rand(1100..1149), :kind => "OEM-HICAP", :printers => [1,2,3]})
seed_consumable ({:desc => "Yellow Toner", :order => Random.rand(1150..1199), :kind => "OEM", :printers => [1,2,3]})
seed_consumable ({:desc => "Black Toner", :order => Random.rand(1200..1249), :kind => "OEM-HICAP", :printers => [1,2,3]})
seed_consumable ({:desc => "Cyan Toner", :order => Random.rand(1250..1299), :kind => "OEM", :printers => [1,2,3]})
seed_consumable ({:desc => "Magenta Toner", :order => Random.rand(1300..1349), :kind => "REM-HICAP", :printers => [1,2,3]})
seed_consumable ({:desc => "Yellow Toner", :order => Random.rand(1350..1399), :kind => "OEM", :printers => [1,2,3]})
seed_consumable ({:desc => "Waste Toner Kit", :order => Random.rand(1400..1449), :kind => "OEM", :printers => [1,2,3]})
seed_consumable ({:desc => "Black Toner", :order => Random.rand(1450..1499), :kind => "OEM", :printers => [4,5]})
seed_consumable ({:desc => "Black Toner", :order => Random.rand(1500..1549), :kind => "REM", :printers => [4,5]})
seed_consumable ({:desc => "Black Toner", :order => Random.rand(1550..1599), :kind => "REM-HICAP", :printers => [4,5]})

@@consumable_count = Consumable.count
puts "Added #{@@consumable_count} Consumables."

# Departments
Department.delete_all

%w{ English Maths Science ICT PE Music Drama }.inject(1) do | count, name |
  Department.create! do |d|
    d.name = name
    d.id = count
  end
  puts "#{name} Department with ID #{count} created."
  count += 1
end
@@department_count = Department.count

# Orders
Order.delete_all

def calculate_order_status
  coin = Random.rand(1..4)
  result = case coin
    when 1 then "AWS"
    when 2 then "PEN"
    when 3 then "DIS"
    when 4 then "DEL"
  end
end

x_orders = 25
x_orders.times do
  random_department = Random.rand(1..@@department_count)
  random_consumable = Random.rand(1..@@consumable_count)
  o = Order.create! do | order |
    order.location = "Room #{Random.rand(1..200)}"
    order.department_id = Department.find(random_department).id
    order.consumable_id = Consumable.find(random_consumable).id
    order.order_status = calculate_order_status
    order.price = Consumable.where(:id => order.consumable_id).first.batch_price.price.to_s
  end
  puts "Order with status: #{o.order_status_text} for #{o.location} from #{o.department.name} created."
end
puts "#{x_orders} orders created."

# Admins

Admin.delete_all

Admin.create! do |a|
 a.email = "test@test.com"
 a.password = "password"
 a.password_confirmation = "password"
end


puts '               .   ,
               .\';_.\';
              .       `.
                    _   \\
             .     (.) (.)--._
            .       "   "     `.
           .                   :
                   ._         .\'
          .          `"-.___."
                      `.
         .              .
         .   .  `.       .
         .    `.  `.      .
,,.      .      ` . `.    .
\\W;      .         "`     .
 `.    .\' .               \'
   `--\'    ,    __,..-   \'  .
          .   .\'     `.   `\' ;
          `.   `,      `.  .\'
            "._.\'        `\' It\'s a Moomin.'
puts "\n==============================================================================="
puts "Initial Administrator created, log in with:\nUsername: 'test@test.com' / Password: 'password'"
puts "==============================================================================="