class AddAttachmentImageToPrinterModels < ActiveRecord::Migration
  def self.up

    add_column :printer_models, :image_file_name, :string
    add_column :printer_models, :image_content_type, :string
    add_column :printer_models, :image_file_size, :integer
    add_column :printer_models, :image_updated_at, :datetime

  end

  def self.down

    remove_column :printer_models, :image_file_name
    remove_column :printer_models, :image_content_type
    remove_column :printer_models, :image_file_size
    remove_column :printer_models, :image_updated_at

  end
end
