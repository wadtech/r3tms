class CreateOrders < ActiveRecord::Migration
  def up
    create_table :orders do |t|
      t.string :location
      t.decimal :price, precision: 3, scale: 2
      t.string :order_status
      t.integer :department_id
      t.integer :consumable_id
      
      t.timestamps
    end
  end
  
  def down
    drop_table :orders
  end
end
