class AddPrinterModelIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :printer_model_id, :integer
  end
end
