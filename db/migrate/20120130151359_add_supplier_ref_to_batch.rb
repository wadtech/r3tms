class AddSupplierRefToBatch < ActiveRecord::Migration
  def change
    add_column :batches, :supplier_ref, :string
  end
end
