class CreateBatches < ActiveRecord::Migration
  def up
    create_table :batches do |t|
      t.decimal :price, precision: 3, scale: 2
      t.date :batch_date

      t.timestamps
    end
  end
  
  def down
    drop_table :batches
  end
end
