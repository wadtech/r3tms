class RemoveConsumableIdFromConsumables < ActiveRecord::Migration
  def change
    remove_column :consumables, :consumable_id
  end
end
