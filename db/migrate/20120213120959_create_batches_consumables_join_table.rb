class CreatePricesJoinTable < ActiveRecord::Migration
  def change
    create_table :batch_prices do |t|
      t.references :batch, :consumable, :null => false
      t.decimal :price, :decimal, :precision => 3, :scale => 2
    end 
  end
end
