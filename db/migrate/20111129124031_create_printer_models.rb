class CreatePrinterModels < ActiveRecord::Migration
  def up
    create_table :printer_models do |t|
      t.string :name
      t.integer :manufacturer_id

      t.timestamps
    end
  end
  
  def down
    drop_table :printer_models
  end
end
