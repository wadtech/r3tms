class AddStockToBatchPrices < ActiveRecord::Migration
  def change
    add_column :batch_prices, :stock, :integer
  end
end
