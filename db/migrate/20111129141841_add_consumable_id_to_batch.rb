class AddConsumableIdToBatch < ActiveRecord::Migration
  def change
    add_column :batches, :consumable_id, :integer
  end
end
