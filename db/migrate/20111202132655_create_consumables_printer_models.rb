class CreateConsumablesPrinterModels < ActiveRecord::Migration
  def change
    create_table "consumables_printer_models", :id => false do |t|
      t.column "consumable_id",  :integer, :null => false
      t.column "printer_model_id", :integer, :null => false
    end
  end
end
