class CreateConsumables < ActiveRecord::Migration
  def up
    create_table :consumables do |t|
      t.string :description
      t.string :reorder_no
      t.string :consumable_id
      t.string :kind
      t.integer :supplier_id
      t.integer :order_id

      t.timestamps
    end
  end
  
  def down
    drop_table :consumables
  end
end
