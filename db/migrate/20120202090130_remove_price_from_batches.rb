class RemovePriceFromBatches < ActiveRecord::Migration
  def change
    remove_column :batches, :price
  end
end
