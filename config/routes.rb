R3TMS::Application.routes.draw do
  devise_for :admins
  
  match 'manage/:controller', :controller => ':controller', :action => 'index'
  match 'manage/:controller/:action', :to => ':controller/:action'
  
  match 'manage', :controller => 'orders', :action => 'track'

  match 'tracking', :controller => 'orders', :action => 'track'
  match 'track', :controller => 'orders', :action => 'track'
  
  resources :orders do
    put :complete, :on => :member
  end
  
  resources :consumables

  resources :printer_models

  resources :batches

  resources :suppliers

  resources :departments

  resources :manufacturers

  root :to => 'orders#new'
end
